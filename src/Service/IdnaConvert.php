<?php

namespace Drupal\idna\Service;

use Algo26\IdnaConvert\ToIdn;
use Algo26\IdnaConvert\ToUnicode;

/**
 * IdnaConvert service.
 */
class IdnaConvert implements IdnaConvertInterface {

  /**
   * IdnaConvert Library Class.
   *
   * @var \Mso\IdnaConvert\IdnaConvert
   */
  protected $idna;

  /**
   * Construct.
   */
  public function __construct() {
    $this->idna = new ToIdn();
    $this->unicode = new ToUnicode();
  }

  /**
   * Encode.
   */
  public function encode($input) {
    if (strpos($input, 'xn--') === FALSE) {
      // Check isURL.
      if (strpos($input, "/") || strpos($input, ":") || strpos($input, "*.") === 0) {
        return $this->idna->convertUrl($input);
      }
      // Check isEMAIL.
      elseif (strpos($input, "@") !== FALSE) {
        $name = strstr($input, "@", TRUE);
        $host = substr(strstr($input, "@"), 1);
        return "$name@" . $this->idna->convert($host);
      }
      return $this->idna->convert($input);
    }
    return $input;
  }

  /**
   * Decode.
   */
  public function decode($input) {
    $input = trim($input);
    // Check isURL.
    if (strpos($input, "/") || strpos($input, ":")) {
      return $this->unicode->convertUrl($input);
    }
    // Check isEMAIL.
    elseif (strpos($input, "@") !== FALSE) {
      return $this->unicode->convertEmailAddress($input);
    }
    return $this->unicode->convert($input);
  }

}
